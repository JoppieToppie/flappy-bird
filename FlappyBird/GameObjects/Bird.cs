﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace FlappyBird.GameObjects
{
    class Bird : RotatingSpriteGameObject
    {
        private const float Gravity = 20f;

        private readonly Vector2 _spawnPosition;
        private readonly Vector2 _jumpVelocity = new Vector2(0f, -450f);

        public Bird() : base("spr_bird")
        {
            _spawnPosition = new Vector2(GameEnvironment.Screen.X / 2f - texture.Width / 2f, GameEnvironment.Screen.Y / 2f - texture.Height / 2f);
            Origin = new Vector2(texture.Width / 2f, texture.Height / 2f);
            Reset();
        }

        public override void Reset()
        {
            base.Reset();

            position = _spawnPosition;
            velocity = Vector2.Zero;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            velocity.Y += Gravity;

            AngularDirection = new Vector2(100f, velocity.Y / 4f);
        }

        public override void HandleInput(InputHelper inputHelper)
        {
            base.HandleInput(inputHelper);

            if (inputHelper.KeyPressed(Keys.Space))
            {
                velocity = _jumpVelocity;
            }
        }
    }
}
