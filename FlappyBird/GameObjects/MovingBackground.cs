﻿using Microsoft.Xna.Framework;

namespace FlappyBird.GameObjects
{
    class MovingBackground : GameObjectList
    {
        private readonly int _textureWidth;

        public MovingBackground()
        {
            var leftBackground = new SpriteGameObject("spr_background");
            var rightBackground = new SpriteGameObject("spr_background");

            _textureWidth = leftBackground.texture.Width;

            leftBackground.position = new Vector2(0f, 0f);
            rightBackground.position = new Vector2(_textureWidth, 0f);

            Add(leftBackground);
            Add(rightBackground);

            velocity.X = -100f;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (position.X < -_textureWidth)
            {
                position.X += _textureWidth;
            }
        }
    }
}
