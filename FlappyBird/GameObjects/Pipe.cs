﻿using Microsoft.Xna.Framework;

namespace FlappyBird.GameObjects
{
    class Pipe : GameObjectList
    {
        public Pipe()
        {
            var upperPipe = new SpriteGameObject("spr_pipe_upper");
            var lowerPipe = new SpriteGameObject("spr_pipe_lower");

            upperPipe.position = new Vector2(0f, -525f);
            lowerPipe.position = new Vector2(0f, 265f);

            Add(upperPipe);
            Add(lowerPipe);

            velocity.X = -300f;

            position.X = 700f;
            position.Y = GameEnvironment.Random.Next(200);
        }

        public bool Overlaps(SpriteGameObject other)
        {
            foreach (var gameObject in Children)
            {
                var pipe = (SpriteGameObject) gameObject;

                if (pipe.Overlaps(other))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
