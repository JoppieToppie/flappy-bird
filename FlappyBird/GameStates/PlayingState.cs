﻿using FlappyBird.GameManagement;
using FlappyBird.GameObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FlappyBird.GameStates
{
    class PlayingState : GameObjectList
    {
        private readonly GameObjectList _pipes = new GameObjectList();
        private int _frameCount;
        private readonly Bird _bird;
        private readonly TextGameObject _score;

        public PlayingState()
        {
            _bird = new Bird();
            _score = new TextGameObject("0", Color.White, GameEnvironment.ContentManager.Load<SpriteFont>("GameFont"))
            {
                position = new Vector2(300f, 50f)
            };

            Add(new MovingBackground());
            Add(_bird);
            Add(_pipes);
            Add(_score);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            HandlePipeSpawning();

            var isGameOver = _bird.position.Y < -_bird.texture.Height || _bird.position.Y > GameEnvironment.Screen.Y || IsBirdCollidingWithAnyPipe();

            if (isGameOver)
            {
                SetGameOver(); 
            }

            _score._text = _pipes.Children.Count.ToString();
        }

        private bool IsBirdCollidingWithAnyPipe()
        {
            foreach (var gameObject in _pipes.Children)
            {
                var pipe = (Pipe) gameObject;

                if (pipe.Overlaps(_bird))
                {
                    return true;
                }
            }

            return false;
        }

        private void HandlePipeSpawning()
        {
            _frameCount++;

            // spawn pipe every frame
            if (_frameCount % 100 == 0)
            {
                _pipes.Add(new Pipe());
            }
        }

        void SetGameOver()
        {
            _bird.Reset();
            _pipes.Children.Clear();
            _frameCount = 0;

            // go to game over screen
            GameEnvironment.SwitchTo(2);
        }
    }
}
