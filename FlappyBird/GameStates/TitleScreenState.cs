﻿using Microsoft.Xna.Framework.Input;

namespace FlappyBird.GameStates
{
    class TitleScreenState : GameObjectList
    {
        public TitleScreenState()
        {
            Add(new SpriteGameObject("spr_titlescreen"));
        }

        public override void HandleInput(InputHelper inputHelper)
        {
            base.HandleInput(inputHelper);

            if (inputHelper.KeyPressed(Keys.Space))
            {
                // to go playing state
                GameEnvironment.SwitchTo(1);
            }
        }
    }
}
