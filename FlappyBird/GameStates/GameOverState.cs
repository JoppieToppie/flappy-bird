﻿using Microsoft.Xna.Framework.Input;

namespace FlappyBird.GameStates
{
    class GameOverState : GameObjectList
    {
        public GameOverState()
        {
            Add(new SpriteGameObject("spr_gameover"));
        }

        public override void HandleInput(InputHelper inputHelper)
        {
            base.HandleInput(inputHelper);

            if (inputHelper.KeyPressed(Keys.Space))
            {
                // to go title screen state
                GameEnvironment.SwitchTo(0);
            }
        }
    }
}
