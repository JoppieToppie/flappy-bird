﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FlappyBird.GameManagement
{
    class TextGameObject : GameObject
    {
        public string _text;
        public Color _color;
        public SpriteFont _spriteFont;

        public TextGameObject(string text, Color color, SpriteFont spriteFont)
        {
            _text = text;
            _color = color;
            _spriteFont = spriteFont;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (visible)
            {
                spriteBatch.DrawString(_spriteFont, _text, position, _color);
            }
        }
    }
}
