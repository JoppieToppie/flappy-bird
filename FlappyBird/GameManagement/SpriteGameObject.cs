﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

class SpriteGameObject : GameObject
{
    public Texture2D texture;

    public SpriteGameObject(string assetName)
    {
        if (assetName.Length > 0)
        {
            texture = GameEnvironment.ContentManager.Load<Texture2D>(assetName);
        }
    }

    public override void Draw(SpriteBatch spriteBatch)
    {
        if (visible)
        {
            spriteBatch.Draw(texture, GlobalPosition, Color.White);
        }
    }

    public bool Overlaps(SpriteGameObject other)
    {
        if (!visible)
        {
            return false;
        }

        float w0 = texture.Width,
            h0 = texture.Height,
            w1 = other.texture.Width,
            h1 = other.texture.Height,
            x0 = GlobalPosition.X,
            y0 = GlobalPosition.Y,
            x1 = other.GlobalPosition.X,
            y1 = other.GlobalPosition.Y;

        return !(x0 > x1 + w1 || x0 + w0 < x1 || y0 > y1 + h1 || y0 + h0 < y1);
    }
}

