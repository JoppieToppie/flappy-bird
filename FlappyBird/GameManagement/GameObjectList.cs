﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

class GameObjectList : GameObject
{

    private readonly List<GameObject> _children;

    public List<GameObject> Children
    {
        get { return _children; }
    }

    public GameObjectList()
    {
        _children = new List<GameObject>();
    }

    public void Add(GameObject gameObject)
    {
        gameObject.Parent = this;
        _children.Add(gameObject);
    }

    public void Remove(GameObject gameObject)
    {
        gameObject.Parent = null;
        _children.Remove(gameObject);
    }

    public override void Reset()
    {
        foreach (var gameObject in _children)
        {
            gameObject.Reset();
        }
    }

    public override void Update(GameTime gameTime)
    {
        base.Update(gameTime);

        foreach (var gameObject in _children)
        {
            gameObject.Update(gameTime);
        }
    }

    public override void Draw(SpriteBatch spriteBatch)
    {
        if (visible)
        {
            foreach (var gameObject in _children)
            {
                gameObject.Draw(spriteBatch);
            }
        }
    }

    public override void HandleInput(InputHelper inputHelper)
    {
        foreach (var gameObject in _children)
        {
            gameObject.HandleInput(inputHelper);
        }
    }
}
